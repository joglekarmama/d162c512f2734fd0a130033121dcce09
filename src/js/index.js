import Phaser from 'phaser';
import config from 'visual-config-exposer';

import Game from './Game';
import PreGame from './PreGame';
import PostGame from './PostGame';
import Instructions from './Instructions';
import LeaderBoard from './LeaderBoard';
import PostLeaderBoard from './PostLeaderBoard';

const body = document.querySelector('body');
body.style.backgroundImage = `url(${config.preGame.bg2Img})`;
body.style.backgroundRepeat = 'no-repeat';
body.style.backgroundSize = 'cover';

const CONFIG = {
  type: Phaser.AUTO,
  backgroundColor: '#000000',
  parent: 'phaser-example',
  scale: {
    mode: Phaser.Scale.RESIZE,
    width: 640,
    height: 960,
    min: {
      width: 320,
      height: 480,
    },
    max: {
      width: 740,
      height: 900,
    },
  },
  dom: {
    createContainer: true,
  },
  scene: [PreGame, LeaderBoard, Instructions, Game, PostGame, PostLeaderBoard],
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 300 },
      // debug: true,
      // debugShowBody: true,
      // debugShowStaticBody: true,
    },
  },
};

let game = new Phaser.Game(CONFIG);
